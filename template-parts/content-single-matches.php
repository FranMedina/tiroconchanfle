<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package tcc
 */

?>



	<div class="entry-content">

		<div class="match-info-container" >
			<h2 class="match-name">
				<?php the_title(); ?>
			</h2>
			<p class="match-day">
				<?php the_field('day_of_the_match'); ?>
			</p>

			//si have rows esta vacio en el else decir : "a este pardido no va nadie!! garcas, llamen a la cancha para cancelar."
			<?php	if( have_rows('match_players_repeater') ): ?>
			<h3>Jugadores Confirmados</h3>
				// obtengo id del usuario con $user_id = get_sub_field( 'player_user', false ))
				// imprimo nombre de usuario, haciendo user_load get_user_by('id', $user_id)
			<ul>
			<?php	while ( have_rows('match_players_repeater') ) : the_row();?>
				<?php if (get_sub_field('player_assist') == true):?>
					<li class="match-players">
						$user_id = get_sub_field( 'player_user', false );
						$usuario = get_user_by('ID', $user_id);
						echo $usuario->user_firstname;
					</li>
				<?php endif;?>
			<?php endwhile; ?>
			</ul>

			<h3>Jugadores Sin Confirmar</h3>
			<ul>
			<?php	while ( have_rows('match_players_repeater') ) : the_row();?>
				<?php if (get_sub_field('player_assist') == false):?>
					<li class="match-players">
						$user_id = get_sub_field( 'player_user', false );
						$usuario = get_user_by('ID', $user_id);
						echo $usuario->user_firstname;
					</li>
				<?php endif;?>
			<?php endwhile; ?>
			</ul>

			<?php endif;


			if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 1 ); }?>


			// Muestro ninja forms si cantidad de confirmados == 10
			//NINJA FORM
			// ASISTIS A ESTE PARTIDO PAPA??
			// SI o NO
			// EL ninja form tiene que tener un hidden field con el match_id

		</div>

	</div><!-- .entry-content -->


</article><!-- #post-## -->
